import React from "react";
import "./App.css"; 
import Posts from "./components/Posts/Posts";
  
const handleClick1 = () => {
  window.open("https://www.indeed.com/career-advice/finding-a-job/types-of-carpentry");
};
const handleClick2 = () => {
  window.open("https://www.housecallpro.com/learn/new-plumbing-technology/");
};
const handleClick3 = () => {
  window.open("https://www.growingagreenerworld.com/gardening-tips/");
};

const App = () => {
  return (
    <div className="main-container">
      <h1   align = "center " className="main-heading">
      Abode BLOGs 
      </h1>
      <Posts />
      <div>
    
      <button style={{float: 'left'}}onClick={handleClick1}>Read more</button>
      <button style={{float: 'center'}} class="center" onClick={handleClick2}>Read more</button>
      <button style={{float: 'right'}} onClick={handleClick3}  >Read more</button>
      </div>
    </div>
  );
};
  
export default App;