import img1 from './image/chimney cleaning.jpeg';
import img2 from './image/NewMovein.jpeg';
import img3 from './image/Disinfection services.jpeg';
import img4 from './image/Fridge cleaning.jpg';
import img5 from './image/FullHomeCleaning.jpeg';
import img6 from './image/MattressCleaning.jpeg';
import img7 from './image/CarpetShampooing.jpeg';
import img8 from './image/Microwavecleaning.jpeg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Chimney Cleaning',
            specialization: 'Rs.249',
            desc: 'Cleaning of Chimney',
        },
        {
            id:2,
            img: img2,
            title: 'New Movein',
            specialization: 'Rs.300',
            desc: 'New Movein cleaning',

        },
        {
            id:3,
            img: img3,
            title: 'Disinfection services',
            specialization: 'Rs.500',
            desc: 'Disinfection Services',

        },
        {
            id:4,
            img: img4,
            title: 'Fridge Cleaning',
            specialization: 'Rs.700',
            desc: 'Cleaning of Fridge',
        },
        {
            id:5,
            img: img5,
            title: 'Full Home Cleaning',
            specialization: 'Rs.700',
            desc: 'Cleaning of Full Cleaning',
        },
        {
            id:6,
            img: img6,
            title: 'MattressCleaning',
            specialization: 'Rs.700',
            desc: 'Cleaning of Mattress',
        },
        {
            id:7,
            img: img7,
            title: 'Carpet Shampooing',
            specialization: 'Rs.700',
            desc: 'Shampooing the carpet',
        },
        {
            id:8,
            img: img8,
            title: 'Microwave Cleaning',
            specialization: 'Rs.700',
            desc: 'Cleaning of Microwave',
        },
        
        
       
       
    ]
}
export default data;
