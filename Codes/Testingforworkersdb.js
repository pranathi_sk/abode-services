import React, { Component } from 'react'
import { Table} from "react-bootstrap";
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class App extends Component {

  constructor(){
    super()
    this.state = {
      records : []
    }
  }
  getData(){
      axios.get('http://localhost:3000/fetch/').then((result) => {
        //console.log("recied data : ",result);
        this.setState({
          records : result.data,
        })
    })
  }

  show(x){
      if(x === "yes")
        alert("Choose other person")
      else
        alert("Selected")
  }

  render() {
    return (
      <div>
        <Table 
        size = "sm"
       variant="default"
       style={{ width: "100%", margin: "20px auto"}}
       striped
       bordered
     >
          <thead>
          <tr>
            <th>Name</th>
            <th>Allocated</th>
            {/*<th>Gender</th>
            <th>PW</th>
    <th>DOJ</th>*/}
          </tr>
          </thead>
          <tbody>
            {this.state.records.map((element) =>(
            <tr>
            <td>{element.name}</td>
            <td>{element.alloted}</td>
            <td> <button onClick = {() => this.show(element.alloted)}>Book</button></td>
            {/*<td>{element.gender}</td>
            <td>{element.password}</td>
            <td>{element.joinDate}</td>*/}</tr>
            ))}
          </tbody>
        </Table>
        <button onClick = {()=>this.getData()}>Display Data</button>
      </div>
    )
  }
}
