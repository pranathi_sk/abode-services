import img1 from './image/Haircolor.jpg';
import img2 from './image/KidsHaircut.jpg';
import img3 from './image/Manicure.jpg';
import img4 from './image/MenHaircut.jpg';
import img5 from './image/OilMassage.jpg';
import img6 from './image/Tan.jpeg';
import img7 from './image/Streaks.jpg';
import img8 from './image/Facial.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Hair Colour',
            specialization: 'Rs.249',
            desc: 'colouring the hair',
        },
        {
            id:2,
            img: img2,
            title: 'Kids Haircut',
            specialization: 'Rs.300',
            desc: 'KidsHaircut',

        },
        {
            id:3,
            img: img3,
            title: 'Manicure',
            specialization: 'Rs.500',
            desc: 'Complete hand care',

        },
        {
            id:4,
            img: img4,
            title: 'Men Haircut',
            specialization: 'Rs.700',
            desc: 'MenHaircut',
        },
        {
            id:5,
            img: img5,
            title: 'Oil Massage',
            specialization: 'Rs.500',
            desc: 'Oil Massage',

        },
        {
            id:6,
            img: img6,
            title: 'Tan Removal',
            specialization: 'Rs.300',
            desc: 'Tan Removal Treatment',
        },
        {
            id:7,
            img: img7,
            title: 'Keratin Treatment',
            specialization: 'Rs.500',
            desc: 'Keratin Treatment',

        },
        {
            id:8,
            img: img8,
            title: 'Facial',
            specialization: 'Rs.500',
            desc: 'Papaya nourishing cleanup',

        },
        
       
       
    ]
}
export default data;
