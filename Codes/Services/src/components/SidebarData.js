import React from 'react';
import * as IoIcons from 'react-icons/io';
import * as MdIcons from "react-icons/md";
import * as BsIcons from "react-icons/bs";
export const SidebarData = [
  {
    title: 'Carpenter',
    path: '/',
    icon: <MdIcons.MdCarpenter/>,
    cName: 'nav-text'
  },
  {
    title: 'Electricians',
    path: '/Electrician',
    icon: <MdIcons.MdElectricalServices />,
    cName: 'nav-text'
  },
  {
    title: 'Plumbing',
    path: '/plumber',
    icon: <MdIcons.MdPlumbing />,
    cName: 'nav-text'
  },
  {
    title: 'Painting',
    path: '/painting',
    icon: <MdIcons.MdFormatPaint/>,
    cName: 'nav-text'
  },
  {
    title: 'Cleaning',
    path: '/cleaning',
    icon: <MdIcons.MdCleaningServices />,
    cName: 'nav-text'
  },
  {
    title: 'Saloon & Spa',
    path: '/saloon',
    icon: <BsIcons.BsScissors />,
    cName: 'nav-text'
  }
];