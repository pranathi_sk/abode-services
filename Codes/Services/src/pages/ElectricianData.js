import img1 from './image/Android_CCTV.jpg';
import img2 from './image/Fan.jpg';
import img3 from './image/Inverter.jpg';
import img4 from './image/Light.jpg';
import img5 from './image/MCB.jpg';
import img6 from './image/Switch.jpg';
import img7 from './image/TV.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Android CCTV installation',
            specialization: 'Rs.249',
            desc: 'Installation of android CCTV',
        },
        {
            id:2,
            img: img2,
            title: 'Fan Installation',
            specialization: 'Rs.300',
            desc: 'Installation of Fan',

        },
        {
            id:3,
            img: img3,
            title: 'Inverter Installation',
            specialization: 'Rs.500',
            desc: 'Installation of inverter',

        },
        {
            id:4,
            img: img4,
            title: 'Bulb Installation',
            specialization: 'Rs.700',
            desc: 'Installation of bulb',
        },
        {
            id:5,
            img: img5,
            title: 'MCB fuse installation',
            specialization: 'Rs.500',
            desc: 'Installation of MCB fuse',

        },
        {
            id:6,
            img: img6,
            title: 'Switchboard installation',
            specialization: 'Rs.300',
            desc: 'Installation of Switchboard',
        },
        {
            id:7,
            img: img7,
            title: 'TV repair',
            specialization: 'Rs.500',
            desc: 'Repair of TV',

        },
        
       
       
    ]
}
export default data;
