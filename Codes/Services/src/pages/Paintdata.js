import img1 from './image/HomeMakeover.jpeg';
import img2 from './image/NewMovein.jpeg';
import img3 from './image/RentalPainting.jpeg';
import img4 from './image/WallDamageSeepage.jpeg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Home Makeover',
            specialization: 'Rs.249',
            desc: 'Home Makeover',
        },
        {
            id:2,
            img: img2,
            title: 'New move in',
            specialization: 'Rs.300',
            desc: 'New move in painting',

        },
        {
            id:3,
            img: img3,
            title: 'Rental painting',
            specialization: 'Rs.500',
            desc: 'Rental Painting',

        },
        {
            id:4,
            img: img4,
            title: 'Wall Damage Seepage',
            specialization: 'Rs.700',
            desc: 'Wall Damage Seepage',
        },
        
        
       
       
    ]
}
export default data;
