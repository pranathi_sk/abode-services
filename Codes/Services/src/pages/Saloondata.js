import img1 from './image/Haircolor.jpg';
import img2 from './image/KidsHaircut.jpeg';
import img3 from './image/Manicure.jpg';
import img4 from './image/MenHaircut.jpeg';
import img5 from './image/OilMassage.jpg';
import img6 from './image/Tan.jpeg';
import img7 from './image/Streaks.jpg';
import img8 from './image/Facial.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Shower installation',
            specialization: 'Rs.249',
            desc: 'Installation of Shower',
        },
        {
            id:2,
            img: img2,
            title: 'Tank installation',
            specialization: 'Rs.300',
            desc: 'Intsallation of Tank',

        },
        {
            id:3,
            img: img3,
            title: 'Tap Installation',
            specialization: 'Rs.500',
            desc: 'Installation of tap',

        },
        {
            id:4,
            img: img4,
            title: 'Waterpipe Installation',
            specialization: 'Rs.700',
            desc: 'Installation of water pipe',
        },
        {
            id:5,
            img: img5,
            title: 'Flush tank installation',
            specialization: 'Rs.500',
            desc: 'Installation of flush tank',

        },
        {
            id:6,
            img: img6,
            title: 'Kichen tile gap filling',
            specialization: 'Rs.300',
            desc: 'Filling Kichen title gap ',
        },
        {
            id:7,
            img: img7,
            title: 'Lock Repair',
            specialization: 'Rs.500',
            desc: 'Repair of lock',

        },
        {
            id:8,
            img: img8,
            title: 'Lock Repair',
            specialization: 'Rs.500',
            desc: 'Repair of lock',

        },
        
       
       
    ]
}
export default data;
