
import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Carpenter from './pages/Carpenter';
import Plumber from './pages/Plumber';
import Painting from './pages/Painting';
import Cleaning from './pages/Cleaning';
import Electrician from './pages/Electrician';
import Saloon from './pages/Saloon';
import Payment from './pages/Payment';
import  cod  from './pages/cod';


function App() {
  return (
    <>
     
      <Router>
      <Navbar />
        <Switch>
          <Route path='/' exact component={Carpenter} />
          <Route path='/Electrician' component={Electrician} />
          <Route path='/plumber' component={Plumber} />
          <Route path='/painting' component={Painting} />
          <Route path='/cleaning' component={Cleaning} />
          <Route path='/saloon' component={Saloon} />
          <Route path='/payment' component={Payment}/>
          <Route path='/cod' component={cod}/>
        </Switch>
      </Router>
      

    
    </>
    
  );
}


export default App;