import React,{useState} from 'react';
import Consult from './Consult';
import Saloondata from './Saloondata';

const Saloon = () => {
    const [Filter,setFilter] = useState('');

    const searchText = (event) => {
        setFilter(event.target.value);

    }
    let dataSearch = Saloondata.cardData.filter(item =>{
        return Object.keys(item).some(key=>
            item[key].toString().toLowerCase().includes(Filter.toString().toLowerCase())
            )

    });
    return(
        <div style={{marginLeft:'250px'}}>
        <section className = "py-4 container ">
            
            <div className = "row justify-content-center">
            
                
                <div className="col-12 mb-5">
                    <div className="mb-9 col-4 mx-auto text-center">
                        
                        <input
                           type = "text"
                           className = "from-control"
                           style = {{width:"450px"}}
                           placeholder = "Search"
                           aria-label = "Search"
                           value = {Filter}
                           alignItems = "center"
                           justifyContent="center"
                           onChange={searchText.bind(this)}
                        />

                    
                   </div>
                </div>
                
                
               {dataSearch.map((item,index)=>{
                   return(
                    
                    
                    
                    <div className="col-lg-3 mx-10 mb-4 ">
                    <div className = "card p-0 overflow-hidden h-100 shadow " >
                    
                        <img src = {item.img} height = "200" width = "100"className = "card-img-top"/>
                        <div className = "card-body">
                            <h5 className = "card-title">{item.title}</h5>
                            
                            <p claaName = "card-text">{item.desc}<br/>{item.specialization}</p>
                            <Consult/>
                         </div> 
                    
                      
                    
                    </div>
                    </div>
                
                    
                    
                    

                    

                   )

               }) }
                

            </div>
            
        </section>
        </div>
    )
}  
export default Saloon