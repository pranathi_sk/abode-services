import { useContext, useState, useEffect } from "react";
import React from 'react';
import axios from "axios";
import "./style.css";
import { useHistory } from "react-router-dom";
import "./Home";
import {useLocalStorage} from "./useLocalStorage";
import { UserContext } from "../App";



export default function Login() {
    const initialValues = { email: "", password: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);
    const [records, setRecord] = useState([]);
    const {state, dispatch} = useContext(UserContext);
  

    const history = useHistory();

    useEffect(() => {
        const json = JSON.stringify(formValues);
        localStorage.setItem("formValues", json);

    }, [formValues]);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
        //console.log(formValues);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setFormErrors(validate(formValues));
        //setIsSubmit(true);
        axios.get("http://localhost:3000/fetch")
            .then((res) => {
                setRecord(res.data);
                //console.log(records[0].email)
                var flag = 0;
                for (var i = 0; i < records.length; i++) {
                    if ((records[i].email === formValues.email) && (records[i].password === formValues.password)) {
                        flag = 1;
                        break;
                    }
                }
                console.log(flag);
                if (flag === 0) {
                    alert("Invalid details \n Please try again.");
                    return;
                }
                else {
                    dispatch({type: 'user',payload:true})
                    //alert("Login successful!")
                    history.push("/")
                }

            })
    };

    /*useEffect(() => {
        console.log(formErrors);
        if (Object.keys(formErrors).length === 0 && isSubmit) {
            console.log(formValues);
        }
    }, [formErrors]);
    */


    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.email) {
            errors.email = "Email is required !"
        } else if (!regex.test(values.email)) {
            errors.email = "This is an invalid email !"
        }
        if (!values.password) {
            errors.password = "Password is required !"
        } else if (values.password.length < 6) {
            errors.password = "Password must be atleast 6 characters !"
        }
        return errors
    }


    return (
        <div className="form" >
        <form onSubmit={handleSubmit}>
            <div className="inner">

            <h3 style={{
                    textAlign: "center"}}>Login</h3><br/>
            <div className="form-group">
                <label>Email</label>
                <input type="email" name="email" className="form-control" placeholder="Enter email" value={formValues.email} onChange={handleChange} />
                <p>{formErrors.email}</p>
            </div>

            <div className="form-group">
                <label>Password</label>
                <input type="password" name="password" className="form-control" placeholder="Enter password" value={formValues.password} onChange={handleChange} />
                <p>{formErrors.password}</p>
            </div>

            <div className="form-group">
                <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                    <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                </div>
            </div>
            <br/>
            <button type="submit" className="btn btn-dark btn-lg btn-block">Sign in</button>
            <p className="forgot-password right"><br/>
                Dont have an account ? <a href="/signup">Register</a>
            </p>
        </div>
        </form >
        </div>
    );
}
