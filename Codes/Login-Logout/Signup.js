import axios from "axios";
import React from 'react';
import { useState, useEffect } from "react";
import { FaRegistered } from "react-icons/fa";
import './style.css'
import { UserContext } from "../App";
import { useContext } from "react";
import { useHistory } from "react-router-dom";

export default function SignUp() {
    const initialValues = { firstName: "", lastName: "", email: "", password: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);
    const {state, dispatch} = useContext(UserContext);
    const history = useHistory();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
        //console.log(formValues);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setFormErrors(validate(formValues));
        //setIsSubmit(true)
        if(!formErrors.length){
        register()
        } else {
            alert("error")
            return;
        }
    }
    const register = () => {
        if(formValues.firstName && formValues.lastName && formValues.email && formValues.password){
            axios.get("http://localhost:3000/fetch").then((res) => {
            //console.log(res.data[0]);
            var flag = 0;
            for (var i=0; i < res.data.length; i++) {
                if(res.data[i].email === formValues.email) {
                    alert("User already exists")
                    flag = 1;
                    return;
                }
            }
            if (flag == 0){
                axios.post('http://localhost:3000/register', formValues)
                    .then((res) => {
                        console.log(res.data);
                        alert(formValues.firstName + " " + formValues.lastName + ' registered successfully');
                        dispatch({type: 'user',payload:true})
                        history.push("/")

                    });

            }
            })
        }
        else{
            alert("Invalid input")
        }

    }

    useEffect(() => {
        console.log(formErrors);
        if (Object.keys(formErrors).length === 0 && isSubmit) {
            console.log(formValues);
        }
    }, [formErrors]);
    

    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.firstName) {
            errors.firstName = "First name is required !"
        } else if (values.firstName.length < 1 || typeof values.firstName === 'number') {
            errors.firstName = "Invalid Name !" 
        }
    
        if (!values.lastName) {
            errors.lastName = "Last name is required !"
        } else if (values.lastName.length < 1 || typeof values.lastName === 'number') {
            errors.lastName = "Invalid Name !"
        }
        if (!values.email) {
            errors.email = "Email is required !"
        } else if (!regex.test(values.email)) {
            errors.email = "This is an invalid email !"
        }
        if (!values.password) {
            errors.password = "Password is required !"
        } else if (values.password.length < 6) {
            errors.password = "Password must be atleast 6 characters !"
        }
        return errors
    }

    return (
        <div className="form">
            <form onSubmit={handleSubmit}>
            <div className="inner">
                <h3 style={{ textAlign: "center"}}>Register</h3>
                

                    <div className="form-group">
                        <label>First name</label>
                        <input type="text" name="firstName" className="form-control" placeholder="First name" value={formValues.firstName} onChange={handleChange} />
                        <p color="red">{formErrors.firstName}</p>
                    </div>

                    <div className="form-group">
                        <label>Last name</label>
                        <input type="text" name="lastName" className="form-control" placeholder="Last name" value={formValues.lastName} onChange={handleChange} />
                        <p color="red">{formErrors.lastName}</p>
                    </div>

                    <div className="form-group">
                        <label>Email</label>
                        <input type="email" name="email" className="form-control" placeholder="Enter email" value={formValues.email} onChange={handleChange} />
                        <p>{formErrors.email}</p>
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" name="password" className="form-control" placeholder="Enter password" value={formValues.password} onChange={handleChange} />
                        <p text-color="red">{formErrors.password}</p>
                    </div>

                    <button type="submit" className="btn btn-dark btn-lg btn-block">Register</button>
                    <p className="forgot-password text-right">
                        Already registered <a href="/login">log in?</a>
                    </p>
                </div>
            </form>
        </div>
    );
}