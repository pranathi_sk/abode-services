import img1 from './image/doorRepair.jpg';
import img2 from './image/bedRepair.jpg';
import img3 from './image/furnitureRepair.jpg';
import img4 from './image/hingeRepair.jpg';
import img5 from './image/houserepair2.jpg';
import img6 from './image/HandleInstallation.jpg';
import img7 from './image/LockRepair.jpg';
import img8 from './image/WoodenShelfInstallation.jpg';
import img9 from './image/doctor9.jpg';
import img10 from './image/doctor10.jpg';
const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Door Repair',
            specialization: 'Rs.249',
            desc: 'Repair of a door',
        },
        {
            id:2,
            img: img2,
            title: 'Bed Repair',
            specialization: 'Rs.300',
            desc: 'Repair of bed',

        },
        {
            id:3,
            img: img3,
            title: 'Furniture Repair',
            specialization: 'Rs.500',
            desc: 'Repair of furniture',

        },
        {
            id:4,
            img: img4,
            title: 'Hinge Installation',
            specialization: 'Rs.700',
            desc: 'Installation of hinge',
        },
        {
            id:5,
            img: img5,
            title: 'Door hanger installation',
            specialization: 'Rs.500',
            desc: 'Installation of door or wall hanger',

        },
        {
            id:6,
            img: img6,
            title: 'Handle Replacement',
            specialization: 'Rs.300',
            desc: 'Replacement of one handle ',
        },
        {
            id:7,
            img: img7,
            title: 'Lock Repair',
            specialization: 'Rs.500',
            desc: 'Repair of lock',

        },
        {
            id:8,
            img: img7,
            title: 'Lock Installation',
            specialization: 'Rs.700',
            desc: 'Installation of lock',

        },
        {
            id:9,
            img: img7,
            title: 'Lock Replacement',
            specialization: 'Rs.1000',
            desc: 'Repairing a lock',
        },
        {
            id:10,
            img: img8,
            title: 'Wooden Shelf Installation',
            specialization: 'Rs.1000',
            desc: 'Pediatrician',

        }
    ]
}
export default data;
