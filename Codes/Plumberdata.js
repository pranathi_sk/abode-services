import img1 from './image/Shower.jpg';
import img2 from './image/Tank.jpg';
import img3 from './image/Tap.jpg';
import img4 from './image/Waterpipe.jpg';
import img5 from './image/FlushTank.jpg';
import img6 from './image/Grouting.jpeg';
import img7 from './image/DrainBlocks.jpg';
import img8 from './image/Basin.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Shower installation',
            specialization: 'Rs.249',
            desc: 'Installation of Shower',
        },
        {
            id:2,
            img: img2,
            title: 'Tank installation',
            specialization: 'Rs.300',
            desc: 'Intsallation of Tank',

        },
        {
            id:3,
            img: img3,
            title: 'Tap Installation',
            specialization: 'Rs.500',
            desc: 'Installation of tap',

        },
        {
            id:4,
            img: img4,
            title: 'Waterpipe Installation',
            specialization: 'Rs.700',
            desc: 'Installation of water pipe',
        },
        {
            id:5,
            img: img5,
            title: 'Flush tank installation',
            specialization: 'Rs.500',
            desc: 'Installation of flush tank',

        },
        {
            id:6,
            img: img6,
            title: 'Kichen tile gap filling',
            specialization: 'Rs.300',
            desc: 'Filling Kichen title gap ',
        },
        {
            id:7,
            img: img7,
            title: 'Drain blocks',
            specialization: 'Rs.500',
            desc: 'Fixing Drain blocks',

        },
        {
            id:8,
            img: img8,
            title: 'Basin Installation',
            specialization: 'Rs.500',
            desc: 'Installation of Basin',

        },
        
       
       
    ]
}
export default data;
