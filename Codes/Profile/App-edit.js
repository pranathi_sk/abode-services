import axios from "axios";
import React from 'react';
import { useState, useEffect } from "react";
//import { FaRegistered } from "react-icons/fa";
//import './style.css'
//import { useHistory } from "react-router-dom";

export default function App() {
    const initialValues = { empName: "", email: "", password: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState([]);
    const [isSubmit, setIsSubmit] = useState(false);
    //const history = useHistory();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
        //console.log(formValues);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setFormErrors(validate(formValues));
        //setIsSubmit(true)
        //update1();
        if (formErrors.length === 0) {
            update1();
        } else {
            return;
        }
    }
    const update1 = () => {
        if (formValues.empName  && formValues.email && formValues.password) {
                    axios.put('http://localhost:3000/update', formValues)
                        .then((res) => {
                            console.log(res.data);
                            alert(formValues.empName + ' updated successfully');
                            //history.push("/")

                        });

                }

    }

    useEffect(() => {
        //console.log(formErrors);
        if (Object.keys(formErrors).length === 0 && isSubmit) {
            console.log(formValues);
        }
    }, [formErrors]);


    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.empName) {
            errors.empName = "Emp name is required !"
        } else if (values.empName.length < 1 || typeof values.empName === 'number') {
            errors.empName = "Invalid Name !"
        }

        // if (!values.lastName) {
        //     errors.lastName = "Last name is required !"
        // } else if (values.lastName.length < 1 || typeof values.lastName === 'number') {
        //     errors.lastName = "Invalid Name !"
        // }
        if (!values.email) {
            errors.email = "Email is required !"
        } else if (!regex.test(values.email)) {
            errors.email = "This is an invalid email !"
        }
        if (!values.password) {
            errors.password = "Password is required !"
        } else if (values.password.length < 6) {
            errors.password = "Password must be atleast 6 characters !"
        }
        return errors
    }

    return (
        <div className="form">
            <form onSubmit={handleSubmit}>
                <div className="inner">
                    <h3 style={{ textAlign: "center" }}>Update</h3>


                    <div className="form-group">
                        <label>Emp name</label>
                        {/* <label>Email</label> */}
                        {/* <input type="text" name="email" className="form-control" placeholder="email" value={formValues.email} onChange={handleChange} /> */}
                        <input type="text" name="empName" className="form-control" placeholder="Emp name" value={formValues.empName} onChange={handleChange} />
                        <p color="red">{formErrors.empName}</p>
                    </div>

                    {/* <div className="form-group">
                        <label>Last name</label>
                        <input type="text" name="lastName" className="form-control" placeholder="Last name" value={formValues.lastName} onChange={handleChange} />
                        <p color="red">{formErrors.lastName}</p>
                    </div> */}

                    <div className="form-group">
                        <label>Email</label>
                        <input type="email" name="email" className="form-control" placeholder="Enter email" value={formValues.email} onChange={handleChange} />
                        <p>{formErrors.email}</p>
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" name="password" className="form-control" placeholder="Enter password" value={formValues.password} onChange={handleChange} />
                        <p text-color="red">{formErrors.password}</p>
                    </div>

                    <button type="submit" className="btn btn-dark btn-lg btn-block">Update</button>
                </div>
            </form>
        </div>
    );
}
