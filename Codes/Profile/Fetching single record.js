import React, { Component } from 'react'
import { Table} from "react-bootstrap";
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class App extends Component {

  constructor(){
    super()
    this.state = {
      records : []
    }
  }
  getData(){
      axios.get('http://localhost:3000/fetch1/'+"padma@gmail.com").then((result) => {
        //console.log("recied data : ",result);
        this.setState({
          records : result.data,
        })
    })
  }


  render() {
    return (
      <div>
        <Table 
       variant="default"
       style={{ width: "100%", margin: "20px auto"}}
       striped
       bordered
     >
          <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>PW</th>
            <th>DOJ</th>
          </tr>
          </thead>
          <tbody>
            <tr>
            <td>{this.state.records.empName}</td>
            <td>{this.state.records.email}</td>
            <td>{this.state.records.gender}</td>
            <td>{this.state.records.password}</td>
            <td>{this.state.records.joinDate}</td></tr>
          </tbody>
        </Table>
        <button onClick = {()=>this.getData()}>Display Data</button>
      </div>
    )
  }
}
