import axios from "axios";
import React from 'react';
import { useState, useEffect } from "react";

export default function SignUp () {
    const initialValues = {firstName: "", lastName: "", email: "", password: ""};
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);

    const handleChange = (e) => {
        const {name, value} = e.target;
        setFormValues({ ...formValues, [name]: value });
        console.log(formValues);
    };

    const handleSubmit = async(e) => {
        e.preventDefault();
        
        setFormErrors(validate(formValues));
        setIsSubmit(true);
        const userObject = {
            firstName: formValues.firstName,
            lastName: formValues.lastName,
            email: formValues.email,
            password: formValues.password
        };
        axios.post('http://localhost:3000/register', userObject)
            .then((res) => {
                console.log(res.data)
            });
        
    };

    useEffect(() => {
        console.log(formErrors);
        if (Object.keys(formErrors).length === 0 && isSubmit) {
            console.log(formValues);
        }
    }, [formErrors]);

    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.firstName){
            errors.firstName = "First name is required !"
        }
        if (!values.lastName){
            errors.lastName = "Last name is required !"
        }
        if (!values.email){
            errors.email = "Email is required !"
        }   else if (!regex.test(values.email)){
            errors.email = "This is an invalid email !"
        }
        if (!values.password){
            errors.password = "Password is required !"
        }   else if (values.password.length < 6){
            errors.password = "Password must be atleast 6 characters !"
        }
        return errors
    }

    return (
        <form onSubmit={handleSubmit}>
            <h3>Register</h3>

            <div className="form-group">
                <label>First name</label>
                <input type="text" name = "firstName" className="form-control" placeholder="First name" value={formValues.firstName} onChange={handleChange}/>
                <p>{formErrors.firstName}</p>
            </div>

            <div className="form-group">
                <label>Last name</label>
                <input type="text" name = "lastName" className="form-control" placeholder="Last name" value={formValues.lastName} onChange={handleChange} />
                <p>{formErrors.lastName}</p>
            </div>

            <div className="form-group">
                <label>Email</label>
                <input type="email" name = "email" className="form-control" placeholder="Enter email" value={formValues.email} onChange={handleChange} />
                <p>{formErrors.email}</p>
            </div>

            <div className="form-group">
                <label>Password</label>
                <input type="password" name = "password" className="form-control" placeholder="Enter password" value={formValues.password} onChange={handleChange}/>
                <p>{formErrors.password}</p>
            </div>

            <button type="submit" className="btn btn-dark btn-lg btn-block">Register</button>
            <p className="forgot-password text-right">
                Already registered <a href="#">log in?</a>
            </p>
        </form>
        );
    }