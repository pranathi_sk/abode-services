import img1 from './images/doorRepair.jpg';
import img2 from './images/bedRepair.jpg';
import img3 from './images/furnitureRepair.jpg';
import img4 from './images/hingeRepair.jpg';
import img5 from './images/houserepair2.jpg';
import img6 from './images/HandleInstallation.jpg';
import img7 from './images/LockRepair.jpg';
import img8 from './images/WoodenShelfInstallation.jpg';
const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Door Repair',
            price: 'Rs.249',
            desc: 'Repair of a door',
        },
        {
            id:2,
            img: img2,
            title: 'Bed Repair',
            price: 'Rs.300',
            desc: 'Repair of bed',

        },
        {
            id:3,
            img: img3,
            title: 'Furniture Repair',
            price: 'Rs.500',
            desc: 'Repair of furniture',

        },
        {
            id:4,
            img: img4,
            title: 'Hinge Installation',
            price: 'Rs.700',
            desc: 'Installation of hinge',
        },
        {
            id:5,
            img: img5,
            title: 'Door hanger installation',
            price: 'Rs.500',
            desc: 'Installation of door or wall hanger',

        },
        {
            id:6,
            img: img6,
            title: 'Handle Replacement',
            price: 'Rs.300',
            desc: 'Replacement of one handle ',
        },
        {
            id:7,
            img: img7,
            title: 'Lock Repair',
            price: 'Rs.500',
            desc: 'Repair of lock',

        },
        {
            id:8,
            img: img7,
            title: 'Lock Installation',
            price: 'Rs.700',
            desc: 'Installation of lock',

        },
        {
            id:9,
            img: img7,
            title: 'Lock Replacement',
            price: 'Rs.1000',
            desc: 'Repairing a lock',
        },
        {
            id:10,
            img: img8,
            title: 'Wooden Shelf Installation',
            price: 'Rs.1000',
            desc: 'Wooden shelf installation',

        }
    ]
}
export default data;
