import img1 from './images/Shower.jpg';
import img2 from './images/Tank.jpg';
import img3 from './images/Tap.jpg';
import img4 from './images/Waterpipe.jpg';
import img5 from './images/FlushTank.jpg';
import img6 from './images/Grouting.jpeg';
import img7 from './images/DrainBlocks.jpg';
import img8 from './images/Basin.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Shower installation',
            price: 'Rs.249',
            desc: 'Installation of Shower',
        },
        {
            id:2,
            img: img2,
            title: 'Tank installation',
            price: 'Rs.300',
            desc: 'Intsallation of Tank',

        },
        {
            id:3,
            img: img3,
            title: 'Tap Installation',
            price: 'Rs.500',
            desc: 'Installation of tap',

        },
        {
            id:4,
            img: img4,
            title: 'Waterpipe Installation',
            price: 'Rs.700',
            desc: 'Installation of water pipe',
        },
        {
            id:5,
            img: img5,
            title: 'Flush tank installation',
            price: 'Rs.500',
            desc: 'Installation of flush tank',

        },
        {
            id:6,
            img: img6,
            title: 'Kichen tile gap filling',
            price: 'Rs.300',
            desc: 'Filling Kichen title gap ',
        },
        {
            id:7,
            img: img7,
            title: 'Drain blocks',
            price: 'Rs.500',
            desc: 'Fixing Drain blocks',

        },
        {
            id:8,
            img: img8,
            title: 'Basin Installation',
            price: 'Rs.500',
            desc: 'Installation of Basin',

        },
        
       
       
    ]
}
export default data;
