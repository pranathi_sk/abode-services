import React from 'react';
import FeedBack from 'react-feedback-popup';
import './Feedback.css';

function Feedback() {
    return (
        <div className="App">
            
            <FeedBack
                style={{ zIndex: '1', position: 'fixed', left: '1px!' }}
                position="right"
                numberOfStars={5}
                headerText="Review"
                bodyText="We'd like to hear from you !"
                buttonText="Share your experience"
                handleClose={() => console.log("handleclose")}
                handleSubmit={(data) =>
                    fetch('https://formspree.io/xxxxxx', {
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: 'POST', // or 'PUT'
                        body: JSON.stringify(data),
                    }).then((response) => {
                        if (!response.ok) {
                            return Promise.reject('Thanks for sharing your feeback !');
                        }
                        response.json()
                    }).then(() => {
                        alert('Success!');
                    }).catch((error) => {
                        alert('Thanks for sharing your feedback !', error);
                    })
                }
                handleButtonClick={() => console.log("handleButtonClick")}

            />
        </div>
    );
}

export default Feedback;