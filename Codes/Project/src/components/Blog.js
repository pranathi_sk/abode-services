import React from "react";
import "./Blog.css";
import Posts from "./Posts/Posts";
//import Posts from "../Posts/Posts";

const handleClick1 = () => {
  window.open("https://www.indeed.com/career-advice/finding-a-job/types-of-carpentry");
};
const handleClick2 = () => {
  window.open("https://www.housecallpro.com/learn/new-plumbing-technology/");
};
const handleClick3 = () => {
  window.open("https://www.growingagreenerworld.com/gardening-tips/");
};

  
  
const Blog = () => {
  return (
    <div className="main-container">
      <br/>
      <h1 className="main-heading" style={{textAlign: "center"}}>
        Abode Services Blog 
      </h1>
      <br/>
      <Posts/>
      <div>
      <button class= "lt" onClick={handleClick1}>Read more</button>
      <button  class="ct" onClick={handleClick2}>Read more</button>
      <button class="rt" onClick={handleClick3}  >Read more</button>
      </div>

    </div>
  );
};
  
export default Blog;