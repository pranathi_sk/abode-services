import img1 from './images/Android_CCTV.jpg';
import img2 from './images/Fan.jpg';
import img3 from './images/Inverter.jpg';
import img4 from './images/Light.jpg';
import img5 from './images/MCB.jpg';
import img6 from './images/Switch.jpg';
import img7 from './images/TV.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Android CCTV installation',
            price: 'Rs.249',
            desc: 'Installation of android CCTV',
        },
        {
            id:2,
            img: img2,
            title: 'Fan Installation',
            price: 'Rs.300',
            desc: 'Installation of Fan',

        },
        {
            id:3,
            img: img3,
            title: 'Inverter Installation',
            price: 'Rs.500',
            desc: 'Installation of inverter',

        },
        {
            id:4,
            img: img4,
            title: 'Bulb Installation',
            price: 'Rs.700',
            desc: 'Installation of bulb',
        },
        {
            id:5,
            img: img5,
            title: 'MCB fuse installation',
            price: 'Rs.500',
            desc: 'Installation of MCB fuse',

        },
        {
            id:6,
            img: img6,
            title: 'Switchboard installation',
            price: 'Rs.300',
            desc: 'Installation of Switchboard',
        },
        {
            id:7,
            img: img7,
            title: 'TV repair',
            price: 'Rs.500',
            desc: 'Repair of TV',

        },  
    ]
}
export default data;
