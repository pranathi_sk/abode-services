// import React, { useState, useEffect } from 'react';
// import { NavLink } from 'react-router-dom';
// const Services = () => {

// const [data, setData] = useState([]);
// const [filter, setFilter] = useState(data);
//  const [loading, setLoading] = useState(false);
// let componentMounted = true;

// useEffect(() => {
//  const getServices = async () => {
// setLoading(true);
//  const response = await fetch("https://fakemedicineappapi.com/medicines");
// if (componentMounted) {
// setData(await response.clone().json());
// setFilter(await response.json());
// setLoading(false);
// console.log(filter);
// }

// return () => {
//  componentMounted = false;
//             }
//          }
//         getServices();
//     }, []);

//     return (
//        <div className="hero">
//             <div className="card bg-dark text-black border-0">
//                  <img className="card-img" src="/assets/clean1_bg.jpg" alt="Background" height="670px" />
//                <div className="card-img-overlay d-flex flex-column justify-content-right">
//                      <div className="container">
//                         <h2 className="card-title ">List of Services </h2>
//                     </div>

//                  </div>

//             </div>

//         </div>
//     );
// };

// export default Services;

import React from 'react';
import './services.css';
import Nav from './Nav';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Carpenter from './Carpenter';
import Electrician from './Electrician';
import Plumber from './Plumber';
import Salon from './Salon';
import Painting from './Painting';
import Cleaning from './Cleaning';
import Pets from './Pets';
import Gardening from './Gardening';
import Mechanic from './Mechanic';
import Payment from './Payment';
import Cash from './Cash';

function Services() {
    return (
        <>
            <Router>
                <Nav />
                <Switch>
                    <Route path='/' exact component={Carpenter} />
                    <Route path='/services' exact component={Carpenter} />
                    <Route path='/Electrician' component={Electrician} />
                    <Route path='/Plumber' component={Plumber} />
                    <Route path='/Painting' component={Painting} />
                    <Route path='/Cleaning' component={Cleaning} />
                    <Route path='/Salon' component={Salon} />
                    <Route path='/Pets' component={Pets} />
                    <Route path='/Gardening' component={Gardening} />
                    <Route path='/Mechanic' component={Mechanic} />
                    <Route path='/Payment' component={Payment} />
                    <Route path='/Cash' component={Cash}/>
                </Switch>
            </Router>
        </>

    );
}
export default Services;
