import React from 'react';
import { Container } from 'react-bootstrap';

const TandC = () => {
    return (
        <Container>
            <div class="bg" className=" align-items-center ustify-content-center">
            <br/><br/><br/><br/><br/>
            <h1 style={{color: "Black", textAlign: "center",}}> Terms and Conditions</h1>
            <h4 style={{color: "Black"}} > Terms of Use: </h4>
            <p>Abode Services reserves the right, in its sole discretion, to change, modify or otherwise amend the Terms, and any other documents incorprated by reference herein for complying with legal and regulatory framework and for other legitimate business purposes, at any time. </p>
            <br/><br/>
            <h4 style={{color: "Black"}} > Privacy Policy: </h4>
            <p>Privacy Policy contains information about how users may seek access to and correction of their personal information held by Abode Services and how they may make a privacy complaint. </p>

        </div>
        </Container>
    )
}
export default TandC;