import img1 from './images/InteriorSpa.jpg';
import img2 from './images/Bikewash.jpg';
import img3 from './images/ACservice.jpg';
import img4 from './images/RatRepellent.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Interior Car Spa',
            price: 'Rs.599',
            desc: 'All the interior parts are cleaned',
        },
        {
            id:2,
            img: img2,
            title: 'Bike Wash',
            price: 'Rs.400',
            desc: 'Total foam wash for bike',

        },
        {
            id:3,
            img: img3,
            title: 'AC Services',
            price: 'Rs.500',
            desc: 'Checks internal damage in AC',

        },
        {
            id:4,
            img: img4,
            title: 'Rat & Pests Repellant',
            price: 'Rs.700',
            desc: 'Controlls pests and rats',
        },
        
        
       
       
    ]
}
export default data;