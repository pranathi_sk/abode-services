import React from 'react';
import {  Container } from 'react-bootstrap';

const Contact = () => {
    return (
        <Container>

            <div className=" align-items-center ustify-content-center">
                <h1 style={{
                    color: "LightSkyBlue",
                    textAlign: "center",
                }}>
                    Contact Us Here
                </h1>

                <br/><br/>
                <h4 style={{
                    color: "Black",
                    textAlign: "center",
                }} >Phone Number : 0402345678
                </h4>
                <h4 style={{
                    color: "Black",
                    textAlign: "center",
                }}>Mobile Number : +919876543210
                </h4>
                <h4 style={{
                    color: "Black",
                    textAlign: "center",
                }}>Email Id : user_support@gmail.com
                </h4>
                <h4 style={{
                    color: "Black",
                    textAlign: "center",
                }}>Address : 4-5-632, XYZ Street, New Delhi-123213
                </h4>


            </div>
        </Container>



    )
}

export default Contact;