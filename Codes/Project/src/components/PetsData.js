import img1 from './images/DogWash.jpg';
import img2 from './images/Dressing.jpg';
import img3 from './images/CatSpa.jpg';
import img4 from './images/CatWash.jpg';
import img5 from './images/HairTrim.jpg';
import img6 from './images/NailTrim.jpg';

const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Shampoo wash',
            price: 'Rs.249',
            desc: 'Complete wash for dogs',
        },
        {
            id:2,
            img: img2,
            title: 'Pets Dressing',
            price: 'Rs.300',
            desc: 'Customised dressing for pets',

        },
        {
            id:3,
            img: img3,
            title: 'Pet Spa',
            price: 'Rs.500',
            desc: 'Perfect spa for pets',

        },
        {
            id:4,
            img: img4,
            title: 'Cat Wash',
            price: 'Rs.700',
            desc: 'Soap wash for cats',
        },
        {
            id:5,
            img: img5,
            title: 'Hair Trim',
            price: 'Rs.700',
            desc: 'Neat hair trimming',
        },
        {
            id:6,
            img: img6,
            title: 'Nails Trim',
            price: 'Rs.700',
            desc: 'Nails shaping and trimming for pets',
        }
 
    ]
}
export default data;