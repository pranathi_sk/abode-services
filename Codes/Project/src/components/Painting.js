import React,{useState} from 'react';
import './services.css';
import Book from './Book';
import PaintingData from './PaintingData';
import painting from './images/Painting.gif'

const Painting = () => {
    const [Filter,setFilter] = useState('');

    const searchText = (event) => {
        setFilter(event.target.value);

    }
    let dataSearch = PaintingData.cardData.filter(item =>{
        return Object.keys(item).some(key=>
            item[key].toString().toLowerCase().includes(Filter.toString().toLowerCase())
            )

    });
    return(
        <div style={{marginLeft:'250px'}}>
        <section className = "py-4 container ">
            
            <div className = "row justify-content-center">
                <div className="col-12 mb-5">
                <br/><br/><br/><br/>
                    <div className="mb-9 col-4 mx-auto text-center">
                        {/* <h3>Painting</h3> */}
                        <img src = {painting} className='center' height='120'/>

                        
                        <input
                           type = "text"
                           className = "from-control"
                           style = {{width:"450px"}}
                           placeholder = "Search"
                           aria-label = "Search"
                           value = {Filter}
                           alignItems = "center"
                           justifyContent="center"
                           onChange={searchText.bind(this)}
                        />
                   </div>
                </div>
 
               {dataSearch.map((item,index)=>{
                   return(

                    <div className="col-lg-3 mx-10 mb-4 ">
                    <div className = "Hover card p-0 overflow-hidden h-100 shadow " >
                    
                        <img src = {item.img} height = "200" width = "100"className = "card-img-top"/>
                        <div className = "card-body">
                            <h5 className = "card-title">{item.title}</h5>
                            
                            <p claaName = "card-text">{item.desc}<br/>{item.price}</p>
                            <Book/>
                         </div> 
                    
                    </div>
                    </div>
                   )

               }) }
                

            </div>
            
        </section>
        </div>
    )
}  
export default Painting;
