import img1 from './images/Haircolor.jpg';
import img2 from './images/KidsHaircut.jpeg';
import img3 from './images/Manicure.jpg';
import img4 from './images/MenHaircut.jpeg';
import img5 from './images/OilMassage.jpg';
import img6 from './images/Tan.jpeg';
import img7 from './images/Streaks.jpg';
import img8 from './images/Facial.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Hair Colour',
            price: 'Rs.249',
            desc: 'Customising colors available',
        },
        {
            id:2,
            img: img2,
            title: 'Kids Hair Cut',
            price: 'Rs.300',
            desc: 'Various styles available ',

        },
        {
            id:3,
            img: img3,
            title: 'Manicure',
            price: 'Rs.500',
            desc: 'Complete handcare',

        },
        {
            id:4,
            img: img4,
            title: 'Mens hair cut',
            price: 'Rs.700',
            desc: 'Various styled haircuts available',
        },
        {
            id:5,
            img: img5,
            title: 'Oil Massage',
            price: 'Rs.500',
            desc: 'Provides relaxation',

        },
        {
            id:6,
            img: img6,
            title: 'Tan Removal',
            price: 'Rs.300',
            desc: '100% tan removal',
        },
        {
            id:7,
            img: img7,
            title: 'Hair Streaks',
            price: 'Rs.500',
            desc: 'Customised colored streaks available',

        },
        {
            id:8,
            img: img8,
            title: 'Facial',
            price: 'Rs.500',
            desc: 'Complete clean up',

        },
        
       
       
    ]
}
export default data;
