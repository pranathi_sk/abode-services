import React, { Component } from 'react'
import { Route } from 'react-router-dom';
import { Table} from "react-bootstrap";
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useHistory } from "react-router-dom";
import EditForm from "./EditForm";
import { Link } from 'react-router-dom';


//import { toHaveFormValues } from '@testing-library/jest-dom/dist/matchers';
export default class Profile extends Component {
   

  constructor(){
    super()
    this.state = {
      records : []
    }
  }
  getData(){
      //console.log("entered")
      axios.get('http://localhost:3000/fetch1/'+"pranathi@gmail.com").then((result) => {
        console.log("recied data : ",result.data);
        this.setState({
          records : result.data,
        })
    })
  }

  render() {
    return (
      <div>
          <br/><br/><br/><br/><br/><br/>

        <Table 
       variant="default"
       style={{ width: "100%", margin: "20px auto"}}
       striped
       bordered
     >
          <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Password</th>
          </tr>
          </thead>
          <tbody>
            <tr>
            <td>{this.state.records.firstName}</td>
            <td>{this.state.records.lastName}</td>
            <td>{this.state.records.email}</td>
            <td>{this.state.records.password}</td>
            </tr>
          </tbody>
        </Table>
        <button onClick = {()=>this.getData()}>Display Data</button>
        {/* <Link to='/EditForm' className='btn btn-outline-success' class = "cent">Edit</Link> */}
      </div>
    )
  }
}


