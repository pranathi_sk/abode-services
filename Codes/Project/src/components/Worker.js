import React, { Component } from 'react'
import { Table} from "react-bootstrap";
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Worker extends Component {

  constructor(){
    super()
    this.state = {
      records : []
    }
  }
  getData(){
      axios.get('http://localhost:3000/fetch2/').then((result) => {
        console.log("recieved data : ",result);
        this.setState({
          records : result.data,
        })
    })
  }

  show(x){
      if(x === "yes")
        alert("Sorry! The worker is occupied\n Please select other person")
      else
        alert("You have selected\n Proceed to payment")
  }

  render() {
    return (
        
      <div>
          console.log("Hello")
          <br/><br/><br/><br/><br/><br/>
        <Table 
        size = "sm"
       variant="default"
       style={{ width: "100%", margin: "20px auto"}}
       striped
       bordered
     >
          <thead>
          <tr>
            <th>Name</th>
            <th>Allocated</th>
          </tr>
          </thead>
          <tbody>
            {this.state.records.map((element) =>(
            <tr>
            <td>{element.name}</td>
            <td>{element.alloted}</td>
            <td> <button onClick = {() => this.show(element.alloted)}>Book</button></td>
            </tr>
            ))}
          </tbody>
        </Table>
        <button onClick = {()=>this.getData()}>View Workers</button>
      </div>
    )
  }
}
