import React from 'react';
//import useLocalStorage from './useLocalStorage';
import Payment from './Payment';
import { Route } from 'react-router';
import { useHistory } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link, Redirect} from 'react-router-dom'

function Book(){
    const history = useHistory();
    const submit = () => {
        var x = localStorage.getItem("formValues");
        // console.log(x.email)
        // console.log(x);
        if(!x){
            alert("Proceed for the payment");
            history.push("/worker");
            //localStorage.clear();
        }
        else{
            alert("To book, you have to login first")
            history.push("/login")
            //localStorage.clear();
        }
        //!x ? <Redirect  to='/login'/>:
    }
    return(
    <button type = "submit" onClick = {submit}  className="btn btn-outline-success">Book now</button>

    )
}


export default Book;
