import React from 'react';
import * as BsIcons from 'react-icons/bs';
import * as IoIcons from 'react-icons/io';
import * as MdIcons from "react-icons/md";
export const SidebarData = [
  {
    title: 'Carpenter',
    path: '/',
    icon: <MdIcons.MdCarpenter/>,
    cName: 'nav-text'
  },
  {
    title: 'Electricians',
    path: '/Electrician',
    icon: <MdIcons.MdElectricalServices />,
    cName: 'nav-text'
  },
  {
    title: 'Plumbing',
    path: '/plumber',
    icon: <MdIcons.MdPlumbing />,
    cName: 'nav-text'
  },
  {
    title: 'Painting',
    path: '/painting',
    icon: <MdIcons.MdFormatPaint/>,
    cName: 'nav-text'
  },
  {
    title: 'Cleaning',
    path: '/cleaning',
    icon: <MdIcons.MdCleaningServices />,
    cName: 'nav-text'
  },
  {
    title: 'Saloon & Spa',
    path: '/salon',
    icon: <BsIcons.BsScissors />,
    cName: 'nav-text'
  },
  {
    title: 'Pet Grooming',
    path: '/Pets',
    icon: <i class="fa fa-paw" aria-hidden="true"></i>,
    cName: 'nav-text'
  },
  {
    title: 'Gardening',
    path: '/Gardening',
    icon: <i class="fa fa-leaf" aria-hidden="true"></i>,
    cName: 'nav-text'
  },
  {
    title: 'Mechanics',
    path: '/Mechanic',
    icon: <i class="fa fa-car" aria-hidden="true"></i>,
    cName: 'nav-text'
  }
];
