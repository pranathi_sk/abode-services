import React from "react";
import "./Posts.css";
import Post from "../Post/Post";
const Posts = () => {
  const blogPosts = [
    {
      title: "Types of Carpentry Work",
      body: `Physically active jobs such as carpentry can reward in many ways. Carpenters have varied work that involves several skills and requirements, as well as many options for specialization and career growth in the future.`,
      author: "Indeed Editorial Team",
      imgUrl:
        "https://upload.wikimedia.org/wikipedia/commons/f/fc/Fotothek_df_n-10_0000828.jpg",
      
    },
    {
      title: " New Plumbing Technologies ",
      body: ' If you’re a plumber, knowing what products and new technologies beyond just plumbing software can be a tremendous boon to your business. After all, if you know what’s in demand, you can then offer those new products to your customers.',
      author: "Andrew Nicoletta",
      imgUrl:
        "https://images.ctfassets.net/990581qemznk/8kTjXvdri0VFSmP7lU7oI/4dd56fe0513f367ff41e28f4006eabc9/HouseCallPro_NewPlumbingTechnologies-Searches.png",
      
    },
    {
      title: "Gardening",
      body: 'There’s a lot to explore here, so we’ve gathered some gardening categories to help you dive into your topic of choice. Don’t find what you’re looking for here? Check out our sister site, joegardener.com.', 
      author: "joegardener",
      imgUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5jsUkh-eRW7E0MCXD-2EIhTbSqaYHpwkp4xK3yi_gXc-lAaV2oHxkzXnca82c07SeaCc&usqp=CAU",
      
    },
    
  ];
  return (
    <div className="posts-container">
      {blogPosts.map((post, index) => (
        <Post key={index} index={index} post={post} />
      ))}
      </div>
  );
};
  
export default Posts;

