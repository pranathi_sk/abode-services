import img1 from './images/chimney cleaning.jpeg';
import img2 from './images/NewMovein.jpeg';
import img3 from './images/Disinfection services.jpeg';
import img4 from './images/Fridge cleaning.jpg';
import img5 from './images/FullHomeCleaning.jpeg';
import img6 from './images/MattressCleaning.jpeg';
import img7 from './images/CarpetShampooing.jpeg';
import img8 from './images/Microwavecleaning.jpeg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Chimney Cleaning',
            price: 'Rs.249',
            desc: 'Cleaning of Chimney',
        },
        {
            id:2,
            img: img2,
            title: 'New Movein',
            price: 'Rs.300',
            desc: 'New Movein cleaning',

        },
        {
            id:3,
            img: img3,
            title: 'Disinfection services',
            price: 'Rs.500',
            desc: 'Disinfection Services',

        },
        {
            id:4,
            img: img4,
            title: 'Fridge Cleaning',
            price: 'Rs.700',
            desc: 'Cleaning of Fridge',
        },
        {
            id:5,
            img: img5,
            title: 'Full Home Cleaning',
            price: 'Rs.700',
            desc: 'Entire house cleaning',
        },
        {
            id:6,
            img: img6,
            title: 'MattressCleaning',
            price: 'Rs.700',
            desc: 'Cleaning of Mattress',
        },
        {
            id:7,
            img: img7,
            title: 'Carpet Shampooing',
            price: 'Rs.700',
            desc: 'Shampooing the carpet',
        },
        {
            id:8,
            img: img8,
            title: 'Microwave Cleaning',
            price: 'Rs.700',
            desc: 'Cleaning of Microwave',
        }, 
    ]
}
export default data;
