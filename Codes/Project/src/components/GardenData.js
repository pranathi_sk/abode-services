import img1 from './images/planting.jpg';
import img2 from './images/plantTrim.jpg';
import img3 from './images/potting.jpg';
import img4 from './images/weed.jpg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Planting',
            price: 'Rs.100',
            desc: '6 plants are planted',
        },
        {
            id:2,
            img: img2,
            title: 'Trimming',
            price: 'Rs.250',
            desc: 'Trimming into perfect shapes',

        },
        {
            id:3,
            img: img3,
            title: 'Potting',
            price: 'Rs.500',
            desc: '10 plants are put in pots',

        },
        {
            id:4,
            img: img4,
            title: 'Weed Removal',
            price: 'Rs.700',
            desc: 'Removal of weeds',
        },
        
        
       
       
    ]
}
export default data;