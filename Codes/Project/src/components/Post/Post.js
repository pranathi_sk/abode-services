import React from "react";
import { Link } from "react-router-dom";
import "./Post.css";
const Post = ({ post: { title, body,
imgUrl, author }, index }) => {
  return (  
    <div className="post-container">
      <h3 className="heading">{title}</h3>
      <img className="image" src={imgUrl} alt="post" />
      <p>{body}</p>
      <div className="info">      
        <h6>Written by: {author}</h6>
      </div>
    </div>
  );
};
  
export default Post;
