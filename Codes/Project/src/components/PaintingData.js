import img1 from './images/HomeMakeover.jpeg';
import img2 from './images/NewMovein.jpeg';
import img3 from './images/RentalPainting.jpeg';
import img4 from './images/WallDamageSeepage.jpeg';


const data = {
    cardData:[
        {
            id:1,
            img: img1,
            title: 'Home Makeover',
            price: 'Rs.249',
            desc: 'Home Makeover',
        },
        {
            id:2,
            img: img2,
            title: 'New move in',
            price: 'Rs.300',
            desc: 'New move in painting',

        },
        {
            id:3,
            img: img3,
            title: 'Rental painting',
            price: 'Rs.500',
            desc: 'Rental Painting',

        },
        {
            id:4,
            img: img4,
            title: 'Wall Damage Seepage',
            price: 'Rs.700',
            desc: 'Wall Damage Seepage',
        },
        
        
       
       
    ]
}
export default data;
