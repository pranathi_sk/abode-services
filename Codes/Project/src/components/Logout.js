import { UserContext } from "../App";
import { useHistory } from "react-router-dom";
import { useContext } from "react";
import "./Login";



const Logout = () => {
    const history = useHistory()
    const {state, dispatch} = useContext(UserContext);
    dispatch({type:'user', payload:false})
    history.push("/login")
    return (
        <div></div>
    )
}
export default Logout;