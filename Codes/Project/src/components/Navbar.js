import React from 'react';
import { NavLink } from 'react-router-dom';
import { UserContext } from '../App';
import { useContext } from 'react';

const Navbar = () => {
    const { state, dispatch } = useContext(UserContext);
    const RenderMenu = () => {
        if (state) {
            return (
                <>
                    <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink className="nav-link active" aria-current="page" to="/">
                                Home
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/services">
                                Services
                            </NavLink>
                        </li>

                        <li className="nav-item">
                            <NavLink className="nav-link" to="/blog">
                                Blog
                            </NavLink>
                        </li>

                    </ul>
                    <div className="buttons">
                        <NavLink to="/Profile" className="btn btn-outline-dark ms-2">
                        <i class="fa fa-user" aria-hidden="true"></i>
                            Profile
                        </NavLink>
                        <NavLink to="/Logout" className="btn btn-outline-dark ms-2">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            Logout
                        </NavLink>
                    </div>
                </>
            )
        } else {
            return (
                <>
                    <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink
                                className="nav-link active"
                                aria-current="page"
                                to="/"
                            >
                                Home
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/services">
                                Services
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/blog">
                                Blog
                            </NavLink>
                        </li>

                    </ul>
                    <div className="buttons">
                        <NavLink to="/login" className="btn btn-outline-dark">
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                            Login
                        </NavLink>
                        <NavLink to="/signup" className="btn btn-outline-dark ms-2">
                            <i className="fa fa-user-plus me-1"></i>
                            Sign Up
                        </NavLink>
                    </div>
                </>
            )
        }

    }
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light py-3 shadow-sm fixed-top">
                <div className="container">
                    <NavLink
                        className="navbar-brand fw-bold fs-2"
                        to="/"
                    >
                        <img src="/assets/logo.png" width="60" height="60" className="d-inline-block align-top" alt=""></img>
                        ABODE SERVICES


                    </NavLink>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <RenderMenu />
                    </div>
                </div>
            </nav>
        </div>
    );
};

export default Navbar;