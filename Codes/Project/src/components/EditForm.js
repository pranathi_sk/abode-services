import axios from "axios";
import React from 'react';
import { useState, useEffect } from "react";

export default function EditForm() {
    const initialValues = { firstName: "", email: "", password: "" };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState([]);
    const [isSubmit, setIsSubmit] = useState(false);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setFormErrors(validate(formValues));
        if (formErrors.length === 0) {
            update1();
        } else {
            return;
        }
    }
    const update1 = () => {
        if (formValues.firstName  && formValues.email && formValues.password) {
                    axios.put('http://localhost:3000/update', formValues)
                        .then((res) => {
                            console.log(res.data);
                            alert(formValues.firstName + ' updated successfully');

                        });

                }

    }

    useEffect(() => {
        if (Object.keys(formErrors).length === 0 && isSubmit) {
            console.log(formValues);
        }
    }, [formErrors]);


    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.firstName) {
            errors.firstName = "Emp name is required !"
        } else if (values.firstName.length < 1 || typeof values.firstName === 'number') {
            errors.firstName = "Invalid Name !"
        }
            if (!values.lastName) {
             errors.lastName = "Last name is required !"
         } else if (values.lastName.length < 1 || typeof values.lastName === 'number') {
             errors.lastName = "Invalid Name !"
         }
        if (!values.email) {
            errors.email = "Email is required !"
        } else if (!regex.test(values.email)) {
            errors.email = "This is an invalid email !"
        }
        if (!values.password) {
            errors.password = "Password is required !"
        } else if (values.password.length < 6) {
            errors.password = "Password must be atleast 6 characters !"
        }
        return errors
    }

    return (
        <div className="form">
            <form onSubmit={handleSubmit}>
                <div className="inner">
                    <h3 style={{ textAlign: "center" }}>Update</h3>


                    <div className="form-group">
                        <label>First name</label>
                        {/* <label>Email</label> */}
                        {/* <input type="text" name="email" className="form-control" placeholder="email" value={formValues.email} onChange={handleChange} /> */}
                        <input type="text" name="firstName" className="form-control" placeholder="Emp name" value={formValues.firstName} onChange={handleChange} />
                        <p color="red">{formErrors.firstName}</p>
                    </div>

                    <div className="form-group">
                        <label>Last name</label>
                        <input type="text" name="lastName" className="form-control" placeholder="Last name" value={formValues.lastName} onChange={handleChange} />
                        <p color="red">{formErrors.lastName}</p>
                    </div>

                    <div className="form-group">
                        <label>Email</label>
                        <input type="email" name="email" className="form-control" placeholder="Enter email" value={formValues.email} onChange={handleChange} />
                        <p>{formErrors.email}</p>
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" name="password" className="form-control" placeholder="Enter password" value={formValues.password} onChange={handleChange} />
                        <p text-color="red">{formErrors.password}</p>
                    </div>

                    <button type="submit" className="btn btn-dark btn-lg btn-block">Update</button>
                </div>
            </form>
        </div>
    );
}
