import React from 'react';
import "./Blog.css"
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";

import { FaFacebookSquare, FaYoutube, FaTwitter, FaInstagram, FaGooglePay, FaPaypal, FaAmazonPay, FaPaytm, } from "react-icons/fa";
import {
    Box, Container,
    Row,
    Column,
    Heading,
} from "./FooterStyles";
import { NavLink } from 'react-router-dom';
import Feedback from './Feedback';
const Home = () => {
    return (
        <>
            <div className="Home">
                <div className="card bg-dark text-white border-0">
                    <div class="sliderimage">
                        <AliceCarousel autoPlay autoPlayInterval="1000" infiniteLoop={true}>
                             {/* <img src={"/assets/5pro1.png"} className="sliderimg" alt=""/>   */}
                             <img src={"/assets/0pro.jpeg"} className="sliderimg" alt=""/>
                            <img src={"/assets/1pro.jpg"} className="sliderimg" alt=""/>
                            <img src={"/assets/2pro1.jpg"} className="sliderimg" alt="" />
                            <img src={"/assets/3pro.jpg"} className="sliderimg" alt="" />
                            <img src={"/assets/4pro1.jpg"} className="sliderimg" alt="" />
                            <img src={"/assets/5pro.jpeg"} className="sliderimg" alt=""/>
                        </AliceCarousel>
                    </div>
                    {/* <img className="card-img" src="/assets/clean_bg.jpg" alt="Background" height="1070px" />

                    <div className="card-img-overlay d-flex flex-column justify-content-center">
                        <div className="container">
                            <h5 className="card-title display-3 fw-bolder mb-0">Abode Services</h5>
                            <h2 className="card-title  mb-0">Services at your Doorstep!!</h2>
                            <p className="card-text lead fs-2">
                                Cleaning and house services are provided here!!!!
                            </p>
                        </div>
    </div> */}
                </div> 

                <Feedback />

            </div><Box>
                <h1 style={{
                    color: "LightSkyBlue",
                    textAlign: "center",
                    marginTop: "-50px"
                }}>
                    Abode Services
                </h1>
                <Container>
                    <Row>
                        <Column>
                            <Heading>About Us</Heading>
                            <NavLink className="nav-link" to="/Aim"><i className=" text-white mr-4">Aim</i>
                            </NavLink>
                            <NavLink className="nav-link" to="/Vision"><i className=" text-white mr-4">Help and Support</i>
                            </NavLink>
                            <NavLink className="nav-link" to="/TandC"><i className=" text-white mr-4">Terms and Conditions</i>
                            </NavLink>
                            <NavLink className="nav-link" to="/Contact"><i className=" text-white mr-4">Contact Us</i>
                            </NavLink>
                        </Column>
                        <Column>
                            <Heading>Services</Heading>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">Carpentry</i>
                            </NavLink>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">Electricals</i>
                            </NavLink>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">Painting</i>
                            </NavLink>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">House Cleaning</i>
                            </NavLink>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">Plumbing</i>
                            </NavLink>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">Salon and Spa</i>
                            </NavLink>
                        </Column>
                        <Column>
                            <Heading>Serving in</Heading>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">Hyderabad</i>
                            </NavLink>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">Chennai</i>
                            </NavLink>
                            <NavLink className="nav-link" to="#"><i className="text-white mr-4">Delhi</i>
                            </NavLink>
                            <NavLink className="nav-link" to="#"><i className=" text-white mr-4">Mumbai</i>
                            </NavLink>
                        </Column>
                        <Column>
                            <Heading>Social Media</Heading>
                            <div className="row py-2 d-flex align-items-center">
                                <div className="col-md-12 text center">
                                    <NavLink className="nav-link" to="#"><FaFacebookSquare />
                                    </NavLink>
                                    <NavLink className="nav-link" to="#"><FaYoutube />
                                    </NavLink>
                                    <NavLink className="nav-link" to="#"><FaTwitter />
                                    </NavLink>
                                    <NavLink className="nav-link" to="#"><FaInstagram />
                                    </NavLink>
                                </div>
                            </div>
                        </Column>
                    </Row>
                </Container>
            </Box></>
    );
}

export default Home;