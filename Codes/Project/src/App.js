import './App.css';
//import Location from './components/Location';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Services from './components/Services';
import Aim from './components/Aim';
import Vision from './components/Vision';
import TandC from './components/TandC';
import Contact from './components/Contact';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Login from './components/Login';
import Signup from './components/Signup';
import { createContext, useReducer } from 'react';
import { initialState, reducer } from './components/UseReducer';
import Blog from './components/Blog';
import Logout from './components/Logout';
import Profile from './components/Profile';

export const UserContext = createContext();

const Routing = () => {
  return(
  <Switch>
  <Route exact path="/" component={Home} />
  <Route exact path="/services" component={Services} />
  <Route exact path="/blog" component={Blog} />
  <Route exact path="/Aim" component={Aim}/>
  <Route exact path="/vision" component={Vision} />
  <Route exact path="/tandc" component={TandC} />
  <Route exact path="/contact" component={Contact} />
  <Route exact path="/login" component={Login} />
  <Route exact path="/signup" component={Signup} />
  <Route exact path="/logout" component={Logout}/>
  <Route exact path="/profile" component={Profile}/>
</Switch>
  )
}


const App = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <>
    <Router>
      <UserContext.Provider value={{ state, dispatch }}>
        <Navbar />
        <Routing/>

      
    </UserContext.Provider>
    </Router>
    </>
  )
}

export default App;